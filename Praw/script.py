import praw
import json
from datetime import datetime
import csv

data_list = []

scraping_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
reddit = praw.Reddit(
    client_id='id-of-app',
    client_secret='secret-key-of-app',
    user_agent='app',
    # username='your_username', 
    # password='your_password'   
)
subreddit = reddit.subreddit('CryptoCurrency')
for submission in subreddit.hot(limit=5):
    post_time = datetime.utcfromtimestamp(submission.created_utc).strftime('%Y-%m-%dT%H:%M:%SZ')
    comments = []
    for comment in submission.comments:
        if isinstance(comment, praw.models.Comment):
            comments.append(comment.body)
    submission_data = {
        'title': submission.title,
        'score': submission.score,
        'url': submission.url,
        'selftext': submission.selftext, 
        'post_time': post_time,
        'scraping_time': scraping_time,
        'comments': comments
    }
    data_list.append(submission_data)
    
    
# Saving data scraped in both json and csv format

output_file_path = 'reddit_data.json'

with open(output_file_path, 'w', encoding='utf-8') as json_file:
    json.dump(data_list, json_file, ensure_ascii=False, indent=2)

output_file_path = 'reddit_data.csv'

with open(output_file_path, 'w', newline='', encoding='utf-8') as csv_file:
    csv_writer = csv.DictWriter(csv_file, fieldnames=['title', 'score', 'url','selftext', 'post_time', 'scraping_time', 'comments'])
    csv_writer.writeheader()
    csv_writer.writerows(data_list)

print(f'Data saved to {output_file_path}')

