from apify_client import ApifyClient
import csv

with open("ElonMusk.csv", "w") as cf:
    cw = csv.writer(cf)
    cw.writerow(["Name", "Followers", "Tweet content", "Retweets", "Likes", "url"])

# Initialize the ApifyClient with your API token
client = ApifyClient("apify_api_JRB0ebB0PbiTibq5ESYtKoun8B6uFc1RHkcY")

# Prepare the Actor input
run_input = {
    "handles": ["elonmusk"],
    "tweetsDesired": 100,
    "proxyConfig": { "useApifyProxy": True },
}

# Run the Actor and wait for it to finish
run = client.actor("quacker/twitter-scraper").call(run_input=run_input)

# Fetch and print Actor results from the run's dataset (if there are any)
for item in client.dataset(run["defaultDatasetId"]).iterate_items():
    print(item)
    with open("ElonMusk.csv", "a", newline = "") as cf:
        cw = csv.writer(cf)
        try:
            cw.writerow([item['user']['name'], item['user']['followers_count'], item['full_text'], item['retweet_count'], item["favorite_count"], item['url']])
        except:
            None
